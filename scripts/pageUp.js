const scrollBtn = document.querySelector('.page-up');
scrollBtn.addEventListener('click', scrollUp);
window.addEventListener('scroll', handleScroll);

function scrollUp() {
  window.scrollTo({
    top: 0,
    behavior: 'smooth',
  });
}

function handleScroll(e) {
  let positionScroll = window.scrollY;
  if (positionScroll > 1000) {
    scrollBtn.style.opacity = 1;
  } else {
    scrollBtn.style.opacity = 0;
  }
}
